_indexOf = require('lodash/array/indexOf')

class Events
	constructor: ->
		@_subscribers = {}

	on: (types, fn) ->
		if typeof types is 'string'
			types = types.split(' ')

		for type in types
			@_subscribers[type] ?= []
			arr = @_subscribers[type]
			if _indexOf(fn, arr) is -1 and typeof fn is 'function'
				arr.push( fn )

		return this

	one: (types, fn) ->
		fn.sk_event_one = true
		@on(types, fn)

	off: (types, fn) ->
		if typeof types is 'string'
			types = types.split(' ')

		for type in types
			arr = @_subscribers[type]
			if arr and arr instanceof Array
                if not fn?
                    arr = []
                else if typeof fn is 'function'
                    i = _indexOf(fn, arr)
                    if i > -1
                        arr.splice(i, 1)

                if arr.length is 0
                    delete @_subscribers[type]

		return this

	# type must by simple string ( best performance for many calls like onscroll )
	trigger: (type, data, context = @) ->
		arr = @_subscribers[type]
		oneArr = []
		if arr
			for fn in arr
				# call is much faster than apply
				# data is simple variable or object
				event =
					type: type
					target: @

				fn.call(context, data, event)

				if fn.sk_event_one?
					delete fn.sk_event_one
					oneArr.push(fn)

			if oneArr.length
				for fn in oneArr
					@off(type, fn)

		return this

module.exports = Events
