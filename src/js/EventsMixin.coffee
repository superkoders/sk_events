Events = require('./Events')

module.exports =
	initEvents: ->
		@events = new Events()
		return @

	on: (types, fn) ->
		@events.on(types, fn)
		return @

	off: (types, fn) ->
		@events.off(types, fn)
		return @

	trigger: (type, fn) ->
		@events.trigger(type, fn, @)
		return @
