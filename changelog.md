### 0.0.7 / 12.11.2015
- odstraňování všech listeneru pro zadaný typ .off('typ')

### 0.0.6 / 13.08.2015
- zrušení vazby na jQuery

### 0.0.5 / 16.04.2015
- přídání event objektu
  - možnost zjištění objektu na kterém událost vznikla pokud se používá proxy
  - možnost zjištění typu události pokud se používá jeden hanlder pro více typů

### 0.0.4 / 05.02.2015
- oprava odstraňování listeneru který byl přidán přes **one**.

### 0.0.3 / 05.02.2015
- přidána podpora pro zaregistrování listeneru který se provede jen jednou. Metoda **one**.

### 0.0.2 / 04.02.2015
- přejmenování proměnné **subscribers** na **_subscribers**. Tzn. je privátní.
- správné nastavení returnů v metodách **on, off, trigger**. Aktuálně vrací kontext nad kterým je metoda volána.
- vyčištení **_subscribers** při odstranění události když pro danou událost už neexistuje jiný listener.
- oprava volání konstruktoru **Events** v **EventsMixin**.

### 0.0.1 / 03.02.2015
- vytvoření.
